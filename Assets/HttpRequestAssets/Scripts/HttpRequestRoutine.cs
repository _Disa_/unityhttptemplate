﻿using System.Collections;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;

namespace HttpRequestAssets.Scripts
{
	[System.Serializable]
	[CreateAssetMenu(fileName = "HttpRequestData",  menuName = "Data/HttpRequestData", order = 1)]
	public class HttpRequestAccessData : ScriptableObject
	{
		public string HttpAddress = "127.0.0.1";
		public string ScriptName  = "default.php";
		public string Hash        = "hash";
	}
	
	[System.Serializable]
	public class HttpRequestRoutine
	{
		[SerializeField] private HttpRequestAccessData _httpRequestData;
		[SerializeField] private UnityEventWithString _onError = new UnityEventWithString();

		private Coroutine _coroutine;
		private MonoBehaviour _dataSender;

		public void SetAnalitycsData(int id, string loginName, string testType, string stepName, float time, MonoBehaviour dataSender)
		{
			if (_coroutine != null || dataSender == null)
			{
				return;
			}

			_dataSender = dataSender;
			_coroutine  = dataSender.StartCoroutine(SetAnalitycsDataAsync(id, loginName, testType, stepName, time));
		}

		public void Stop()
		{
			if (_coroutine == null || _dataSender == null)
			{
				return;
			}
			
			_dataSender.StopCoroutine(_coroutine);
			_coroutine = null;
		}

		private IEnumerator SetAnalitycsDataAsync(int id, string loginName, string testType, string stepName, float time)
		{
			var form  =  new WWWForm();
			
			form.AddField("form_hash", _httpRequestData.Hash);
			
			form.AddField("form_id",        id);
			form.AddField("form_loginName", loginName);
			form.AddField("form_testType",  testType);
			form.AddField("form_stepName",  stepName);
			form.AddField("form_time",      time.ToString(CultureInfo.InvariantCulture));
			
			var request = new WWW(_httpRequestData.HttpAddress + "/" + _httpRequestData.ScriptName, form);
			yield return request;

			if (request.error != null)
			{
				Debug.LogErrorFormat("SetAnalitycsDataAsync error: {0}", request.error);
				_onError.Invoke(request.error);
			}
			else
			{
				var resultText = request.text;

				if (resultText.Contains("Error") && !resultText.Contains("Warning"))
				{
					Debug.LogErrorFormat("SetAnalitycsDataAsync error: {0}", resultText);
					_onError.Invoke(resultText);
				}
				else
				{
					Debug.LogFormat("SetAnalitycsDataAsync success : {0}", resultText);
				}
				request.Dispose();
			}

			_coroutine = null;
		}
	}

	[System.Serializable]
	public class UnityEventWithString :  UnityEvent<string>
	{
	}
}
