﻿using System.Globalization;
using UnityEngine;

namespace HttpRequestAssets.Scripts
{
    public class HttpRequestManager : MonoBehaviour
    {
        [SerializeField] private HttpRequestRoutine _httpRequestRoutine = new HttpRequestRoutine();
        
        // *** FOR DEBUG ***
        public bool _showDebugUi;
        
        private string _id;
        private string _loginName;
        private string _testType;
        private string _stepName;
        private string _time;
        // *** END DEBUG ***
          
        public void SetAnalitycsData(int id, string loginName, string testType, 
                                     string stepName, float time)
        {
            _httpRequestRoutine.SetAnalitycsData(id, loginName, testType, stepName, time, this);
        }

        private void OnDestroy()
        {
            _httpRequestRoutine.Stop();
        }

        private void OnGUI()
        {
            if (!_showDebugUi)
            {
                return;
            }

            GUILayout.BeginVertical("box");
            {
                _id        = GUILayout.TextField(_id);
                _loginName = GUILayout.TextField(_loginName);
                _testType  = GUILayout.TextField(_testType);
                _stepName  = GUILayout.TextField(_stepName);
                _time      = GUILayout.TextField(_time);

                if (GUILayout.Button("Send"))
                {
                    var id   = int.Parse(_id, CultureInfo.InvariantCulture.NumberFormat);
                    var time = float.Parse(_time, CultureInfo.InvariantCulture.NumberFormat);

                    SetAnalitycsData(id, _loginName, _testType, _stepName, time);
                }
            }
            GUILayout.EndVertical();
        }
    }
}
